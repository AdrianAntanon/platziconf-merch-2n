import React from 'react';

import '../styles/components/Footer.css';

const Footer = () => {
  return (
    <div>
      <p className="Footer-title">Platzi Conf Merch</p>
      <p className="Footer-copy">Lado izquierdo reservado</p>
    </div>
  );
};

export default Footer;
